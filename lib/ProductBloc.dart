import 'dart:async';

import 'package:blocproductexample/Bloc.dart';

class ProductBloc extends Bloc {
  List<String> _list = ["Mobile", "Laptop", "Keyboard", "Mouse"];

  StreamController<List<String>> _productListStreamController =
      StreamController<List<String>>();

  Stream<List<String>> get productListStream =>
      _productListStreamController.stream;

  ProductBloc() {
    _productListStreamController.sink.add(_list);
  }

  void deleteProduct(String name) {
    _list.remove(name);
    _productListStreamController.sink.add(_list);
  }

  void addNewProduct(String name) {
    _list.add(name);
    _productListStreamController.sink.add(_list);
  }

  @override
  void dispose() {
    _productListStreamController.close();
  }
}
