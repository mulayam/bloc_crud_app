import 'package:blocproductexample/ProductBloc.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ProductBloc _productBloc = new ProductBloc();

  TextEditingController _textEditingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bloc"),
      ),
      floatingActionButton: FloatingActionButton(
        child: IconButton(
          icon: Icon(Icons.add),
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text("Add New Product"),
                  content: TextField(
                    autofocus: true,
                    controller: _textEditingController,
                  ),
                  actions: <Widget>[
                    RaisedButton(
                      child: Text("SAVE"),
                      onPressed: () {
                        String name = _textEditingController.text;
                        _productBloc.addNewProduct(name);
                        _textEditingController.text = "";
                      },
                    )
                  ],
                );
              },
            );
          },
        ),
      ),
      body: StreamBuilder<List<String>>(
        stream: _productBloc.productListStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<String> list = snapshot.data;
            return ListView.builder(
              itemCount: list.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 0,
                  child: ListTile(
                    title: Text(list[index]),
                    trailing: IconButton(
                      onPressed: () {
                        _productBloc.deleteProduct(list[index]);
                      },
                      icon: Icon(
                        Icons.delete,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                );
              },
            );
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
